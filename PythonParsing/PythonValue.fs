﻿namespace PythonParsing

type PythonValue =
  | Src of PythonValue list
  | Func of (string * int)
//    | Int of int
//    | Float of float
//    | Bool of bool
  | String of string
  | Typedarglist
  | Test
  | Tfpdef of string
  | Parameters