﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

open Microsoft.FSharp.Text.Lexing
open PythonParsing

[<EntryPoint>]
let main argv = 
    let parse python = 
        let lexbuf = LexBuffer<char>.FromString python
        let res = Parser.start Lexer.read lexbuf
        res
    let python = "def name(): def func():"
    let (Some parseResult) = python |> parse
    printfn "%s" ("Completed")
    0 // return an integer exit code
